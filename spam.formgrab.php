<?php

namespace Brandnewbox\Formgrab\Service;

use EllisLab\Addons\Spam\Service\SpamModerationInterface;

/**
 * Moderate Spam for the Formgrab module
 */
class Formgrab_spam implements SpamModerationInterface {

	/**
	 * Approve Trapped Spam
	 * Posts the submission
	 *
	 * @param  object $sub EllisLab\ExpressionEngine\Model\FormSubmission
	 * @param  string $extra
	 * @return void
	 */
	public function approve($sub, $extra)
	{
		$sub->save();
	}

	/**
	 * Reject Trapped Spam
	 *
	 * @param  object $sub EllisLab\ExpressionEngine\Model\FormSubmission
	 * @param  string $extra
	 * @return void
	 */
	public function reject($sub, $extra)
	{
		$sub->delete();
	}
}
// END CLASS

// EOF
